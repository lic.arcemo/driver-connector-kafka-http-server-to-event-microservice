# Driver Connector - Http Server to Kafka (Event Microservices)
## _API Gateway to Event Microservice_

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Driver Connector is a package thant you can use to connect a http server to event microservices with kafka message broker.
## INSTALLATION

## CREATE NODE.CONFIG FILES:

```sh
cd api-gateway
touch node.config
```
insert next code:
```
bootstrap.servers=[server]:[port]
security.protocol=SASL_SSL
sasl.mechanisms=PLAIN
sasl.username=[string]
sasl.password=[password]
session.timeout.ms=45000
```
```sh
cd transactions
touch node.config
```
insert the same code.

## RUN API GATEWAY:

```sh
cd api-gateway
npm i
node server.js -f ./node.config -t api-gateway
```


## RUN EVENT MICROSERVICE (TRANSACTIONS):

```sh
cd transactions
npm i
node server.js -f ./node.config -t transactions
```

API GATEWAY is running in port 3000

you can send transactions to api gateway [POST] http://localhost:3000/transaction

body
```
{
	"destination":"transactions",
	"data":{}
}
```

const {driver, EmiterReceive} = require('./driver');

class DriverService {


    async connect () {
        driver.startConsumer();
        let prod =  await driver.startProducer();
        await driver.setProducer(prod);
        return {prod, send: driver};
    }

    getRespond(){
        let response =  driver.response;
        return response;
    }
}
module.exports = { DriverService, EmiterReceive }
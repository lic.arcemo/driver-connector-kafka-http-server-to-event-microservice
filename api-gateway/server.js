const {DriverService, EmiterReceive} = require('./driver/main');
const { mainController } = require('./controllers/accounts/mainController');
const driver = new DriverService();
const express = require('express')
const bodyParser = require("body-parser");
const router = express.Router();
const app = express()
const port = 3000
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


async function main(){

    
    let { send } = await driver.connect();

    //api gateway not receive request by topic, only receive request by http
    EmiterReceive.on( 'listen', async function (res) {
           // send.response(res, res.origin);
    });


    router.post('/transaction', async (req, res) => {
        let data = req.body;
        const idtx = Math.floor(Math.random() * 1000);
        data.idtx = idtx;
        //console.log(data);
        var a = await send.request(data, data.destination);
       // console.log(a);
        res.json(a);
      })

      app.use("/", router);
      
      app.listen(port, () => {
        console.log(`Example app listening on port ${port}`)
      });
      
    
    
   

    // setTimeout( async () => {
    //     for (let idx = 0; idx < 5; ++idx) {
    //         const key = 'A';
    //         const value = Buffer.from(JSON.stringify({ type: 'REQUEST',idtx:1, origin:'orchestrator'}));
    //        var a = await send.request({idtx:1}, 'api-gateway');
    //        console.log(a);
    //       }
    // }, 10000);

    
}

main();




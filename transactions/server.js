const {DriverService, EmiterReceive} = require('./driver/main');
const { mainController } = require('./controllers/accounts/mainController');
const driver = new DriverService();


async function main(){

    
    let { send } = await driver.connect();

    EmiterReceive.on( 'listen', async function (res) {
        
            const random = Math.floor(Math.random() * 10);
            await setTimeout(() => {
                res.timeout = random*1000;
                data = res;
                console.log(data);
                send.response(data, res.origin);
            }, random*1000);
    });
}

main();




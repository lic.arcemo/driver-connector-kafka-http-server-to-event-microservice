/* Copyright 2020 Confluent Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * =============================================================================
 *
 * Produce messages to Confluent Cloud
 * Using the node-rdkafka client for Apache Kafka
 *
 * =============================================================================
 */ 

const Kafka = require('node-rdkafka');
const { configFromCli } = require('./config');
const EventEmitter = require('events');
class Emitter extends EventEmitter {};
const emitter     = new Emitter();;
const EmiterReceive     = new Emitter();
let config = null;

async function createProducer(config, onDeliveryReport) {
    
  const producer = new Kafka.Producer({
    'bootstrap.servers': config['bootstrap.servers'],
    'sasl.username': config['sasl.username'],
    'sasl.password': config['sasl.password'],
    'security.protocol': config['security.protocol'],
    'sasl.mechanisms': config['sasl.mechanisms'],
    'dr_msg_cb': true
  });
  

  return new Promise((resolve, reject) => {
    producer
      .on('ready', () => resolve(producer))
      .on('delivery-report', onDeliveryReport)
      .on('event.error', (err) => {
        console.warn('event.error', err);
        reject(err);
      });
    producer.connect();
  });
}


function createConsumer(config, onData) {
    const consumer = new Kafka.KafkaConsumer({
      'bootstrap.servers': config['bootstrap.servers'],
      'sasl.username': config['sasl.username'],
      'sasl.password': config['sasl.password'],
      'security.protocol': config['security.protocol'],
      'sasl.mechanisms': config['sasl.mechanisms'],
      'group.id': 'node-example-group-1'
    }, {
      'auto.offset.reset': 'earliest'
    });
  
    return new Promise((resolve, reject) => {
      consumer
        .on('ready', () => resolve(consumer))
        .on('data', onData);
  
      consumer.connect();
    });
  }
  

let prod = null;

service = {
    startProducer: async function() {
        config = await configFromCli();
        if (config.usage) {
            return console.log(config.usage);
        }

        const producer = await createProducer(config, (err, report) => {
            if (err) {
            console.warn('Error producing', err)
            } else {
            const {topic, partition, value} = report;
            console.log(`Successfully produced record to topic "${topic}" partition ${partition} ${value}`);
            }
        });

        process.on('SIGINT', () => {
            console.log('\nDisconnecting producer ...');
            producer.disconnect();
          });

        return producer;
    },
    startConsumer: async function() {
        const config = await configFromCli();
    
        if (config.usage) {
          return console.log(config.usage);
        }
    
        console.log(`Consuming records from ${config.topic}`);
    
        let seen = 0;
    
        const consumer = await createConsumer(config, ({key, value, partition, offset}) => {
            console.log(`Consumed record with key ${key} and value ${value} of partition ${partition} @ offset ${offset}. Updated total count to ${++seen}`);
            let data = JSON.parse(value);
            switch (true) {
                case (data.type ==='REQUEST'):
                    EmiterReceive.emit( 'listen' , JSON.parse(value)); 
                    break;
                case (data.type ==='RESPONSE'):
                    console.log(data.type);
                    emitter.emit( data.idtx, data);
                    break;
            
                default:
                    break;
            }
         
        });
    
        consumer.subscribe([config.topic]);
        consumer.consume();
    
        process.on('SIGINT', () => {
          console.log('\nDisconnecting consumer ...');
          consumer.disconnect();
        });
      },
    setProducer: async function(producer){
        prod = producer
        return prod;
    },

    response: async function (data, destination){

        data.origin = config.topic;
        data.destination = destination;
        data.type = 'RESPONSE';
        const key = config.topic;
        const value = Buffer.from(JSON.stringify(data));
        prod.produce(data.destination, -1, value, key);

    },
    request: async function (data, destination){
        
        data.origin = config.topic;
        data.destination = destination;
        data.type = 'REQUEST';
        const key = config.topic;
        const value = Buffer.from(JSON.stringify(data));

        return new Promise((resolve,reject)=> {
            prod.produce(data.destination, -1, value, key);
            emitter.once( data.idtx, res => {
                resolve(res);
            });
        })

        //prod.produce(data.destination, -1, value, key);

       
    }
}

// produceExample()
//   .catch((err) => {
//     console.error(`Something went wrong:\n${err}`);
//     process.exit(1);
//   });

  module.exports =  {driver: service, EmiterReceive}; 
